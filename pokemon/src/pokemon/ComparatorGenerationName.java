package pokemon;

import java.util.Comparator;

public class ComparatorGenerationName implements Comparator<Pokemon> {
    
    @Override
    public int compare(Pokemon n1, Pokemon n2) {
        int result = n1.getName().compareTo(n2.getName());
        return result != 0 ? result : Integer.compare(n1.getCode(), n2.getCode());
    }
}