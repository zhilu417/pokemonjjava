package pokemon;

import java.io.*;
import java.util.*;

public class FileManager {
	

	public String readTotals(String fileInput, String type1, String type2) {
		List<String> filteredPokemons = new ArrayList<>();
		try (Scanner scanner = new Scanner(new File(fileInput))) {
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] fields = line.split(";");

				String pokemonType1 = fields[2];
				String pokemonType2 = fields[3];

				if ((type1.isEmpty() || pokemonType1.equalsIgnoreCase(type1))
						&& (type2.isEmpty() || pokemonType2.equalsIgnoreCase(type2))) {
					filteredPokemons.add(line);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		StringBuilder result = new StringBuilder();
		result.append("Code;Name;Type1;Type2;Total\n");
		for (String pokemon : filteredPokemons) {
			result.append(pokemon).append("\n");
		}
		return result.toString();

	}

	public void write(String text, String fileOutput) {
		try (PrintWriter writer = new PrintWriter(new FileWriter(fileOutput))) {
			writer.println(text);
			System.out.println("FILE " + fileOutput + " CREATED.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<Pokemon> searchPokemons(String fileInput, String pokemonName) {
		List<Pokemon> foundPokemons = new ArrayList<>();

		try (Scanner scanner = new Scanner(new File(fileInput))) {
			scanner.nextLine();

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] fields = line.split(";");

				String name = fields[1];

				if (name.toLowerCase().contains(pokemonName.toLowerCase())) {
					Pokemon pokemon = new Pokemon();
					pokemon.setCode(Integer.parseInt(fields[0]));
					pokemon.setName(fields[1]);
					pokemon.setType1(fields[2]);
					pokemon.setType2(fields[3]);
					pokemon.setHealthPoints(Integer.parseInt(fields[4]));
					pokemon.setAttack(Integer.parseInt(fields[5]));
					pokemon.setDefense(Integer.parseInt(fields[6]));
					pokemon.setSpecialAttack(Integer.parseInt(fields[7]));
					pokemon.setSpecialDefense(Integer.parseInt(fields[8]));
					pokemon.setSpeed(Integer.parseInt(fields[9]));
					pokemon.setGeneration((byte) Integer.parseInt(fields[10]));
					pokemon.setLegendary(Boolean.parseBoolean(fields[11]));

					foundPokemons.add(pokemon);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return foundPokemons;
	}

	public void write(List<Pokemon> pokemons, String fileOutput) {
		try (PrintWriter writer = new PrintWriter(new FileWriter(fileOutput))) {
			writer.println(
					"Code;Name;Type1;Type2;HealthPoints;Attack;Defense;SpecialAttack;SpecialDefense;Speed;Generation;Legendary");
			for (Pokemon pokemon : pokemons) {
				writer.println(pokemon.getCode() + ";" + pokemon.getName() + ";" + pokemon.getType1() + ";"
						+ pokemon.getType2() + ";" + pokemon.getHealthPoints() + ";" + pokemon.getAttack() + ";"
						+ pokemon.getDefense() + ";" + pokemon.getSpecialAttack() + ";" + pokemon.getSpecialDefense()
						+ ";" + pokemon.getSpeed() + ";" + pokemon.getGeneration() + ";" + pokemon.isLegendary());
			}
			System.out.println("FILE " + fileOutput + " CREATED.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<Pokemon> orderLegendary(String fileInput, byte legendary) {
		List<Pokemon> allPokemons = new ArrayList<>();
		List<Pokemon> filteredPokemons = new ArrayList<>();

		try (Scanner scanner = new Scanner(new File(fileInput))) {
			scanner.nextLine();

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] fields = line.split(";");

				Pokemon pokemon = new Pokemon();
				pokemon.setCode(Integer.parseInt(fields[0]));
				pokemon.setName(fields[1]);
				pokemon.setType1(fields[2]);
				pokemon.setType2(fields[3]);
				pokemon.setHealthPoints(Integer.parseInt(fields[4]));
				pokemon.setAttack(Integer.parseInt(fields[5]));
				pokemon.setDefense(Integer.parseInt(fields[6]));
				pokemon.setSpecialAttack(Integer.parseInt(fields[7]));
				pokemon.setSpecialDefense(Integer.parseInt(fields[8]));
				pokemon.setSpeed(Integer.parseInt(fields[9]));
				pokemon.setGeneration((byte) Integer.parseInt(fields[10]));
				pokemon.setLegendary(Boolean.parseBoolean(fields[11]));

				allPokemons.add(pokemon);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		for (Pokemon pokemon : allPokemons) {
			if ((legendary == 1 && pokemon.isLegendary()) || (legendary == 2 && !pokemon.isLegendary())) {
				filteredPokemons.add(pokemon);
			}
		}
//        Collections.sort(filteredPokemons);
		return filteredPokemons;
	}

	public List<Pokemon> orderType(String fileInput) {
		List<Pokemon> pokemons = new ArrayList<>();

		try (Scanner scanner = new Scanner(new File(fileInput))) {
			scanner.nextLine();

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] fields = line.split(";");

				Pokemon pokemon = new Pokemon();
				pokemon.setCode(Integer.parseInt(fields[0]));
				pokemon.setName(fields[1]);
				pokemon.setType1(fields[2]);
				pokemon.setType2(fields[3]);
				pokemon.setHealthPoints(Integer.parseInt(fields[4]));
				pokemon.setAttack(Integer.parseInt(fields[5]));
				pokemon.setDefense(Integer.parseInt(fields[6]));
				pokemon.setSpecialAttack(Integer.parseInt(fields[7]));
				pokemon.setSpecialDefense(Integer.parseInt(fields[8]));
				pokemon.setSpeed(Integer.parseInt(fields[9]));
				pokemon.setGeneration((byte) Integer.parseInt(fields[10]));
				pokemon.setLegendary(Boolean.parseBoolean(fields[11]));

				pokemons.add(pokemon);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		Comparator<Pokemon> comparatorType = new Comparator<Pokemon>() {
			@Override
			public int compare(Pokemon p1, Pokemon p2) {
				int compareType1 = p1.getType1().compareTo(p2.getType1());
				if (compareType1 != 0) {
					return compareType1;
				} else {
					return p1.getType2().compareTo(p2.getType2());
				}
			}
		};

		Collections.sort(pokemons, comparatorType);
		return pokemons;
	}

	public List<Pokemon> orderGenerationName(String fileInput) {
		List<Pokemon> pokemons = new ArrayList<>();

		try (Scanner scanner = new Scanner(new File(fileInput))) {
			scanner.nextLine();

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] fields = line.split(";");

				Pokemon pokemon = new Pokemon();
				pokemon.setCode(Integer.parseInt(fields[0]));
				pokemon.setName(fields[1]);
				pokemon.setType1(fields[2]);
				pokemon.setType2(fields[3]);
				pokemon.setHealthPoints(Integer.parseInt(fields[4]));
				pokemon.setAttack(Integer.parseInt(fields[5]));
				pokemon.setDefense(Integer.parseInt(fields[6]));
				pokemon.setSpecialAttack(Integer.parseInt(fields[7]));
				pokemon.setSpecialDefense(Integer.parseInt(fields[8]));
				pokemon.setSpeed(Integer.parseInt(fields[9]));
				pokemon.setGeneration((byte) Integer.parseInt(fields[10]));
				pokemon.setLegendary(Boolean.parseBoolean(fields[11]));

				pokemons.add(pokemon);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		Comparator<Pokemon> comparatorGenerationName = new Comparator<Pokemon>() {
			@Override
			public int compare(Pokemon p1, Pokemon p2) {
				int compareGeneration = Integer.compare(p1.getGeneration(), p2.getGeneration());
				if (compareGeneration != 0) {
					return compareGeneration;
				} else {
					return p1.getName().compareTo(p2.getName());
				}
			}
		};

		Collections.sort(pokemons, comparatorGenerationName);
		return pokemons;
	}

	public List<Pokemon> extractGeneration(String fileInput, byte generation) {
		List<Pokemon> generationPokemons = new ArrayList<>();

		try (Scanner scanner = new Scanner(new File(fileInput))) {
			// Skip header line
			scanner.nextLine();

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] fields = line.split(";");

				byte pokemonGeneration = Byte.parseByte(fields[10]);

				if (pokemonGeneration == generation) {
					Pokemon pokemon = new Pokemon();
					pokemon.setCode(Integer.parseInt(fields[0]));
					pokemon.setName(fields[1]);
					pokemon.setType1(fields[2]);
					pokemon.setType2(fields[3]);
					pokemon.setHealthPoints(Integer.parseInt(fields[4]));
					pokemon.setAttack(Integer.parseInt(fields[5]));
					pokemon.setDefense(Integer.parseInt(fields[6]));
					pokemon.setSpecialAttack(Integer.parseInt(fields[7]));
					pokemon.setSpecialDefense(Integer.parseInt(fields[8]));
					pokemon.setSpeed(Integer.parseInt(fields[9]));
					pokemon.setGeneration((byte) Integer.parseInt(fields[10]));
					pokemon.setLegendary(Boolean.parseBoolean(fields[11]));

					generationPokemons.add(pokemon);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return generationPokemons;
	}

	public boolean deleteFile(String string) {
		return false;
	}

}
