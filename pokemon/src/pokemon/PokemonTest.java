package pokemon;

import java.io.*;
import java.util.*;

public class PokemonTest {
	public static Scanner scanner = new Scanner(System.in);	
	public static String PATH = "src" + File.separator + "pokemon" + File.separator;
	public static String FILE = "pokemons.csv";
	public static String FILE_TOTALS = "pokemonsTotals.csv";
	public static String FILE_SEARCH = "pokesonsSearch.csv";
	public static String FILE_GENERATION = "pokesonsGeneration";
	public static String FILE_LEGENDARY = "pokesonsLegendary.csv";
	public static String FILE_GEN_NAME = "pokesonsGerase. csv";
	public static String FILE_TYPE = "pokemonsType.csv";
	public static String FILE_EXT = ".csv";

	public static void main(String[] args) {
		FileManager file = new FileManager();
		byte option = 1;
		while (option != 0) {
			option = menu();
			if (option == 0) {
				System.out.println("ADIOS");
			}
			switch (option) {
			case 1: {
				System.out.print("TOTALES \nINGRESE EL TIPO1 (PRESIONE ENTER PARA TODOS): ");
				String type1 = scanner.next();
				System.out.print("\nINGRESE EL TIPO2 (PRESIONE ENTER PARA TODOS): ");
				String type2 = scanner.next();
				System.out.print("\nINGRESE LA RUTA DEL ARCHIVO (PRESIONE ENTER PARA EL PREDETERMINADO): ");
				String filePath = scanner.next();
				String text = file.readTotals((filePath.isEmpty() ? PATH + FILE : filePath), type1, type2);
				if (text != "") {
					file.write(text, PATH + FILE_TOTALS);
					System.out.println("ARCHIVO " + FILE_TOTALS + " CREADO.");
				} else {
					if (file.deleteFile(PATH + FILE_TOTALS)) {
						System.out.println(
								"ARCHIVO " + FILE_TOTALS + " BORRADO. NO HAY POKÉMONS CON ESTAS ESPECIFICACIONES.");
					} else {
						System.out.println("ARCHIVO " + FILE_TOTALS
								+ " ESTÁ ABIERTO Y NO SE PUEDE BORRAR. NO HAY POKÉMONS CON ESTAS ESPECIFICACIONES.");
					}
				}
				break;
			}
			case 2: {
				System.out.print("BÚSQUEDA DE POKÉMON POR NOMBRE\nINGRESE EL NOMBRE: ");
				String name = scanner.next();
				System.out.print("\nINGRESE LA RUTA DEL ARCHIVO (PRESIONE ENTER PARA EL PREDETERMINADO): ");
				String filePath = scanner.next();
				List<Pokemon> text = file.searchPokemons((filePath.isEmpty() ? PATH + FILE : filePath), name);
				if (!text.isEmpty()) {
					file.write(text, PATH + FILE_TOTALS);
					System.out.println("ARCHIVO " + FILE_TOTALS + " CREADO.");
				} else {
					if (file.deleteFile(PATH + FILE_TOTALS)) {
						System.out.println(
								"ARCHIVO " + FILE_TOTALS + " BORRADO. NO HAY POKÉMONS CON ESTAS ESPECIFICACIONES.");
					} else {
						System.out.println("ARCHIVO " + FILE_TOTALS
								+ " ESTÁ ABIERTO Y NO SE PUEDE BORRAR. NO HAY POKÉMONS CON ESTAS ESPECIFICACIONES.");
					}
				}
				break;
			}
			case 3: {
				System.out.print("EXTRACCIÓN DE GENERACIÓN\nELIJA LA GENERACIÓN: ");
				file.extractGeneration(PATH + FILE, scanner.nextByte());
				break;
			}
			case 4: {
				System.out.print("ORDENAR POR LEGENDARIO\nELIJA LEGENDARIO 1.VERDADERO, 2.FALSO:");
				file.orderLegendary(PATH + FILE, scanner.nextByte());
				break;
			}
			case 5: {
				System.out.print("ORDENAR POR TIPO1 Y TIPO2:");
				file.orderType(PATH + FILE);
				break;
			}
			case 6: {
				System.out.print("ORDENAR POR GENERACIÓN Y NOMBRE:");
				file.orderGenerationName(PATH + FILE);
				break;
			}
			default:
				System.out.println(" ERROR,INTÉNTALO DE NUEVO");
			}

		}
	}

	private static byte menu() {
		System.out.println("MENU");
	    System.out.println("1. TOTALS");
	    System.out.println("2. POKEMON SEARCH");
	    System.out.println("3. EXTRACT GENERATION");
	    System.out.println("4. ORDER LEGENDARY");
	    System.out.println("5. ORDER BY TYPE1 AND TYPE2");
	    System.out.println("6. ORDER BY GENERATION AND NAME");
	    System.out.println("0. QUIT");
	    System.out.print("CHOOSE MENU OPTION: ");
	    return scanner.nextByte();
	}

	private byte dataEntryByte(String a, byte c, byte d) {
		return d;

	}

	private int dataEntryInt(String d) {
		byte c = 0;
		return c;

	}

	private double dataEntryDouble(String d) {
		byte c = 0;
		return c;

	}

	private float dataEntryFloat(String d) {
		byte c = 0;
		return c;

	}

	private String dataEntryString(String d) {
		return d;

	}
}