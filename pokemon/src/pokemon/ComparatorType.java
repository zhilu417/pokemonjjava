package pokemon;

import java.util.Comparator;


public class ComparatorType implements Comparator<Pokemon> {
	
	@Override
	  public int compare(Pokemon o1, Pokemon o2) {
	        int result = String.CASE_INSENSITIVE_ORDER.compare(o1.getType1(), o2.getType1());
	        if (result == 0) {
	            result = String.CASE_INSENSITIVE_ORDER.compare(o1.getType2(), o2.getType2());
	        }
	        return result;
	    }
	}


